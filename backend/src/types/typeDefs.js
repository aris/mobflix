import { gql } from "apollo-server";

const typeDefs = gql`
    type Movies {
        identity: String
        labels: String
        duration: String
        listed_in: String
        date_added: String
        country: String
        show_id: String
        director: String
        rating: String
        release_year: String
        description: String
        type: String
        title: String
    }

    type User {
        name: String!
    }

    type Mutation {
        AddFavouriteMovie(userName: String!, title: String!): Movies
            @cypher(
                statement: """
                MATCH (user:User {name: $userName})
                MATCH (movie:Movies {title: $title})
                MERGE (user) - [f:favouriteMovies] -> (movie)
                RETURN movie
                """
            )
    }

    type Mutation {
        RemoveFavouriteMovie(userName: String!, title: String!): Movies
            @cypher(
                statement: """
                MATCH (user:User {name: $userName})
                MATCH (movie:Movies {title: $title})
                MATCH (user) - [f:favouriteMovies] -> (movie)
                DELETE f
                """
            )
    }

    type Query {
        FindFavouriteMovie(userName: String!): [Movies]
            @cypher(
                statement: """
                MATCH (user:User {name: $userName})
                MATCH p=(user)-[r:favouriteMovies]->(movie)
                RETURN movie
                """
            )
    }
`;
export default typeDefs;
