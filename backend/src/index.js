import { ApolloServer } from "apollo-server";
import typeDefs from "./types/typeDefs.js";
import neo4j from "neo4j-driver";
import dotenv from "dotenv";
import { Neo4jGraphQL } from "@neo4j/graphql";

/**
 * Sets the enviroment variabels from the .env file
 */
dotenv.config();

/**
 * create a Neo4j driver instance, that connects to the database
 * with the Uri, password and username from the enviroment variabels.
 */
const driver = neo4j.driver(process.env.DB_URI, neo4j.auth.basic(process.env.DB_USER, process.env.DB_PASSWORD));

/**
 * Creates a shcema with our typedefs and driver
 */
const neo4jGraphQL = new Neo4jGraphQL({ typeDefs, driver });

/**
 * Starts an Apolloserver
 */
neo4jGraphQL
    .getSchema()
    .then((schema) => {
        const server = new ApolloServer({
            schema: schema,
        });

        server.listen({ port: 4000 }).then(({ url }) => {
            console.log(`🚀 Server ready at ${url}`);
        });
    })
    .catch((err) => console.log(err));
